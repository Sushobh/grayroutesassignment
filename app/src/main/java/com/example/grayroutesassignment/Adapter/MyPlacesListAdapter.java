package com.example.grayroutesassignment.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.grayroutesassignment.Interfaces.MyPlaceClickListener;
import com.example.grayroutesassignment.Models.MyPlace;
import com.example.grayroutesassignment.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Daily programmer on 10/15/2017.
 */

public class MyPlacesListAdapter extends RecyclerView.Adapter<MyPlacesListAdapter.MyViewHolder> {


    MyPlaceClickListener myPlaceClickListener;
    ArrayList<MyPlace> myPlaces;

    public MyPlacesListAdapter(MyPlaceClickListener myPlaceClickListener, ArrayList<MyPlace> myPlaces) {
        this.myPlaceClickListener = myPlaceClickListener;
        this.myPlaces = myPlaces;
    }

    @Override
    public MyPlacesListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_place,parent,false));
    }

    @Override
    public void onBindViewHolder(final MyPlacesListAdapter.MyViewHolder holder, int position) {
         final MyPlace myPlace = myPlaces.get(position);
         Picasso.with((Context) myPlaceClickListener).load(new File(myPlace.getFilePath())).fit().centerCrop().into(holder.imageView);
         holder.latitude.setText("Latitude "+myPlace.getLatitude());
         holder.longitude.setText("Longitude "+myPlace.getLongitude());
    }

    @Override
    public int getItemCount() {
        return myPlaces.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;
        TextView latitude;
        TextView longitude;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            latitude = (TextView)itemView.findViewById(R.id.latitude);
            longitude = (TextView)itemView.findViewById(R.id.longitude);
        }

        @Override
        public void onClick(View view) {
            myPlaceClickListener.clickedOnMyPlace(myPlaces.get(getAdapterPosition()));
        }
    }

    public static Bitmap getBitmap(String filepath, ImageView imageView){
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filepath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(filepath, bmOptions);
        return bitmap;
    }
}
