package com.example.grayroutesassignment;

/**
 * Created by Daily programmer on 10/14/2017.
 */

public class Constant {
    public static final String FILE_PROVIDER_AUTHORITY = "com.example.grayroutesassignment";
    public static final float SELECTED_PLACE_ZOOM = 4;
}
