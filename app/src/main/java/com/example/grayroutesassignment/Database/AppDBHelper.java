package com.example.grayroutesassignment.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.grayroutesassignment.Models.MyPlace;

import java.util.ArrayList;

/**
 * Created by Daily programmer on 10/14/2017.
 */

public class AppDBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "UserPlace.db";
    private SQLiteDatabase db;
    private static AppDBHelper instance;


    private AppDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();
    }

    public static synchronized AppDBHelper getHelper(Context context)
    {
        if (instance == null){
            instance = new AppDBHelper(context);
        }


        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(UserPlaceContract.TABLE_DEFINITION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(UserPlaceContract.DELETE_ENTRIES_USER_PLACE);
        onCreate(db);

    }

    public void savePlace(MyPlace myPlace){
        ContentValues values = new ContentValues();
        values.put(UserPlaceContract.UserPlace.LATITUDE,myPlace.getLatitude());
        values.put(UserPlaceContract.UserPlace.LONGITUDE,myPlace.getLongitude());
        values.put(UserPlaceContract.UserPlace.FILEPATH,myPlace.getFilePath());
        long newRowId = db.insert(UserPlaceContract.UserPlace.TABLE_NAME, null, values);
    }


}
