package com.example.grayroutesassignment.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.grayroutesassignment.Models.MyPlace;

import java.util.ArrayList;

/**
 * Created by Daily programmer on 10/14/2017.
 */

public class Database {

    Context mContext;
    SQLiteDatabase db;


    public Database(Context mContext){
       this.mContext = mContext;
       db = AppDBHelper.getHelper(mContext).getWritableDatabase();
    }


    public void savePlace(MyPlace myPlace){
        ContentValues values = new ContentValues();
        values.put(UserPlaceContract.UserPlace.LATITUDE,myPlace.getLatitude());
        values.put(UserPlaceContract.UserPlace.LONGITUDE,myPlace.getLongitude());
        values.put(UserPlaceContract.UserPlace.FILEPATH,myPlace.getFilePath());
        long newRowId = db.insert(UserPlaceContract.UserPlace.TABLE_NAME, null, values);
    }

    public ArrayList<MyPlace> getAllAddedPlaces(){
        ArrayList<MyPlace> myPlaces = new ArrayList<>();
        Cursor cursor = db.query(
                UserPlaceContract.UserPlace.TABLE_NAME,                     // The table to query
                null,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );
        while(cursor.moveToNext()) {
            MyPlace myPlace = new MyPlace();
            int itemId = cursor.getInt(
                    cursor.getColumnIndexOrThrow(UserPlaceContract.UserPlace._ID));
            String latitude =
                    cursor.getString(cursor.getColumnIndexOrThrow(UserPlaceContract.UserPlace.LATITUDE));
            String longitude =
                    cursor.getString(cursor.getColumnIndexOrThrow(UserPlaceContract.UserPlace.LONGITUDE));
            String filepath =
                    cursor.getString(cursor.getColumnIndexOrThrow(UserPlaceContract.UserPlace.FILEPATH));
            myPlace.setId(itemId);
            myPlace.setFilePath(filepath);
            myPlace.setLongitude(longitude);
            myPlace.setLatitude(latitude);
            myPlaces.add(myPlace);
        }
        cursor.close();
        return myPlaces;
    }

}
