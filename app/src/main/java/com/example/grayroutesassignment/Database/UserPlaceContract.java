package com.example.grayroutesassignment.Database;

import android.provider.BaseColumns;

/**
 * Created by Daily programmer on 10/14/2017.
 */

public final class UserPlaceContract {

    private UserPlaceContract(){

    }

    public static final String TABLE_DEFINITION = "create table "+UserPlace.TABLE_NAME+" " +
            "("+UserPlace._ID+" integer primary key ," +
            UserPlace.LATITUDE+" TEXT ," +
            UserPlace.LONGITUDE+" TEXT ,"+
            UserPlace.FILEPATH +" TEXT )";

    public static final String DELETE_ENTRIES_USER_PLACE =
            "DROP TABLE IF EXISTS " + UserPlace.TABLE_NAME;


    public static final class UserPlace implements BaseColumns{
        public static final String TABLE_NAME = "places";
        public static final String LONGITUDE = "longitude";
        public static final String LATITUDE = "latitude";
        public static final String FILEPATH = "filepath";
    }

}
