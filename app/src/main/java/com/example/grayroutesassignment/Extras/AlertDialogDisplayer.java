package com.example.grayroutesassignment.Extras;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;

/**
 * Created by Daily programmer on 10/15/2017.
 */

public class AlertDialogDisplayer {

    public static void displayAlertDialog(Context mContext, String title, String message, final DialogInterface.OnDismissListener onDismissListener ){
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setCancelable(true);
        builder.setOnDismissListener(onDismissListener);
        builder.setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });
        builder.show();
    }
}
