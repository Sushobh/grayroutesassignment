package com.example.grayroutesassignment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import com.example.grayroutesassignment.Interfaces.ActivityResultListener;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by Daily programmer on 10/14/2017.
 */

public class ImageCapturer implements ActivityResultListener {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    String mCurrentPhotoPath;



    public interface ImageCapturerListener{
        void onCapturedImage(File imageFile);
        void onFailedToCaptureImage(String errorMessage);
    }

    private ImageCapturerListener imageCapturerListener;
    private Activity mContext;

    public ImageCapturer(ImageCapturerListener imageCapturerListener, Context mContext) {
        this.imageCapturerListener = imageCapturerListener;
        this.mContext = (Activity) mContext;
    }

    public void openCameraToTakePicture(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(mContext.getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
                Uri photoURI = FileProvider.getUriForFile(mContext,
                        Constant.FILE_PROVIDER_AUTHORITY,
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                mContext.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            } catch (IOException ex) {
                 imageCapturerListener.onFailedToCaptureImage(ex.getMessage());
            }

        }
    }

    private File createImageFile() throws IOException {

        File storageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                UUID.randomUUID().toString(),
                ".jpg",
                storageDir
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onReceivedActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE){
            if(resultCode == Activity.RESULT_OK){
                imageCapturerListener.onCapturedImage(new File(mCurrentPhotoPath));
            }
            else
            {
                imageCapturerListener.onFailedToCaptureImage("Failed to get image from camera");
            }
        }
    }
}
