package com.example.grayroutesassignment.Interfaces;

import android.content.Intent;

/**
 * Created by Daily programmer on 10/14/2017.
 */

public interface ActivityResultListener {
    void onReceivedActivityResult(int requestCode,int resultCode,Intent data);
}
