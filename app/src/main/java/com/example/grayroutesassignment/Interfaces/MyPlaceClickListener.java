package com.example.grayroutesassignment.Interfaces;

import com.example.grayroutesassignment.Models.MyPlace;

/**
 * Created by Daily programmer on 10/15/2017.
 */

public interface MyPlaceClickListener {
    void clickedOnMyPlace(MyPlace myPlace);
}
