package com.example.grayroutesassignment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;


import com.example.grayroutesassignment.Extras.AlertDialogDisplayer;
import com.example.grayroutesassignment.Models.MyPlace;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.example.grayroutesassignment.R.id.map;

/*
  This screen displays the map with all of the selcted places as markers.
 */

public class MainActivity extends SuperActivity implements OnMapReadyCallback, ImageCapturer.ImageCapturerListener, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private LatLng currentlySelectedLatLng;
    private ImageCapturer imageCapturer;
    private ArrayList<Marker> markers;



    /*
     Add custom behavior when the back button is clicked in the action bar.
     */

    @Override
    protected void clickedOnBackButtonInActionBar() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);
        imageCapturer = new ImageCapturer(this,this);

        /*
          Add an observer which will receive result activity result updates.
          In this case , the image capturer will receive the activity result from the camera application.
         */
        addActivityResultListener(imageCapturer);

        getSupportActionBar().setTitle("Gray Routes Assignment");


    }

    private void addTagsToMap() {
        markers = new ArrayList<>();
        if(mMap != null){
            for(MyPlace myPlace : database.getAllAddedPlaces()){
                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(myPlace.getLatLng()));
                marker.setTag(myPlace.getId());
                markers.add(marker);
            }
            mMap.setOnMarkerClickListener(this);
        }
        /*
          If no places have been added , show a welcome message
         */
        if(!(markers.size()>0)){
            showAlertMessage();
        }
        setSelectedPlaceIfPresent(getIntent());
    }

    private void showAlertMessage() {
        AlertDialogDisplayer.displayAlertDialog(this, "Welcome", "To start adding places long click on any point on the map!", new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                currentlySelectedLatLng = latLng;
                imageCapturer.openCameraToTakePicture();
            }
        });
        addTagsToMap();
    }

    @Override
    public void onCapturedImage(File imageFile) {
        saveTagData(imageFile,currentlySelectedLatLng);
        addTagsToMap();
    }

    private void saveTagData(File imageFile, LatLng currentlySelectedLatLng) {
        /*
         Save the selected tag info to database.
         */
        if(currentlySelectedLatLng != null){
            MyPlace myPlace = new MyPlace();
            myPlace.setFilePath(imageFile.getAbsolutePath());
            myPlace.setLatitude(String.valueOf(currentlySelectedLatLng.latitude));
            myPlace.setLongitude(String.valueOf(currentlySelectedLatLng.longitude));
            database.savePlace(myPlace);
        }
    }

    @Override
    public void onFailedToCaptureImage(String errorMessage) {
       showToast("Failed to capture message");
    }

    public void viewSavedPlaces(View view) {
         startActivity(new Intent(this,MyPlacesActivity.class));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        /*
          We have a single instance of this activity, so when this activity is launched again,
          we receive the new intent here....
         */
        setSelectedPlaceIfPresent(intent);
        super.onNewIntent(intent);
    }

    public void setSelectedPlaceIfPresent(Intent intent){
        if(intent.hasExtra(StringKeys.SELECTED_PLACE)){
            MyPlace myPlace = (MyPlace) intent.getSerializableExtra(StringKeys.SELECTED_PLACE);
            for(Marker marker : markers){
                if(marker.getTag().equals(myPlace.getId())){
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), Constant.SELECTED_PLACE_ZOOM));
                }
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
}
