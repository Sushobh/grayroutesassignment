package com.example.grayroutesassignment.Models;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * Created by Daily programmer on 10/14/2017.
 */

public class MyPlace implements Serializable{

    int id;
    String latitude;
    String longitude;
    String filePath;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    public LatLng getLatLng(){
        return new LatLng(Double.valueOf(getLatitude()),Double.valueOf(getLongitude()));
    }
}
