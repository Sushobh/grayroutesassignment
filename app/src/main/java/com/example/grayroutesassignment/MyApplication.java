package com.example.grayroutesassignment;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by Daily programmer on 10/14/2017.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        /*
         This library is to monitor the sqlite database and shared preferences on the chrome developer tools.
         */
        Stetho.initializeWithDefaults(this);
    }
}
