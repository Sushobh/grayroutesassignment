package com.example.grayroutesassignment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.example.grayroutesassignment.Adapter.MyPlacesListAdapter;
import com.example.grayroutesassignment.Interfaces.MyPlaceClickListener;
import com.example.grayroutesassignment.Models.MyPlace;

import java.util.ArrayList;

/*
  This screen displays a list of places that the user has added and on Click of any item,
  opens the map with the selected place at the center.

 */

public class MyPlacesActivity extends SuperActivity implements MyPlaceClickListener {

    private RecyclerView myPlacesListView;
    private MyPlacesListAdapter myPlacesAdapter;


    @Override
    protected void clickedOnBackButtonInActionBar() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_places);

        myPlacesListView = (RecyclerView)findViewById(R.id.list_view);
        myPlacesListView.setLayoutManager(new LinearLayoutManager(this));


        displayMyPlaces();
        getSupportActionBar().setTitle("Saved Places");
    }


    private void displayMyPlaces() {
        /*
         Fetched selected places and display.
         If no places are present then show show a message.
         */
        ArrayList<MyPlace> places = database.getAllAddedPlaces();
        if(places.size()>0){
            myPlacesAdapter = new MyPlacesListAdapter(this,places);
            myPlacesListView.setAdapter(myPlacesAdapter);
        }
        else {
            findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void clickedOnMyPlace(MyPlace myPlace) {
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra(StringKeys.SELECTED_PLACE,myPlace);
        startActivity(intent);
        finish();
    }

}
