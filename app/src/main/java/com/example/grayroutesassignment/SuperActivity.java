package com.example.grayroutesassignment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;


import com.example.grayroutesassignment.Database.Database;
import com.example.grayroutesassignment.Interfaces.ActivityResultListener;

import java.util.ArrayList;

/**
 * Created by Daily programmer on 10/14/2017.
 */

public abstract class SuperActivity extends AppCompatActivity {

    private ArrayList<ActivityResultListener> activityResultListeners;
    protected Database database;
    protected abstract void clickedOnBackButtonInActionBar();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = new Database(this);
        activityResultListeners = new ArrayList<>();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void showToast(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    protected void addActivityResultListener(ActivityResultListener activityResultListener){
        activityResultListeners.add(activityResultListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for(ActivityResultListener activityResultListener : activityResultListeners){
            activityResultListener.onReceivedActivityResult(requestCode,resultCode,data);
        }
    }

    public static void log(String message){
        Log.i("MyApp",message);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                clickedOnBackButtonInActionBar();
        }
        return true;
    }

}
